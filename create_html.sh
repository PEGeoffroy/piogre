#!/bin/bash

dir="./public/projects/blog/article"

articles=$(find ./public/projects/blog/article -type f -printf "%f\n")

touch ./public/projects/blog/article/tag.txt

for entry in "$dir"/*.adoc
do
  filename=$(basename -- "$entry")
  extension="${filename##*.}"
  filename="${filename%.*}"

  line=$(head -n 1 $entry)

  echo "$filename.$extension | $line" >> ./public/projects/blog/article/tag.txt

  title="$(tr '[:lower:]' '[:upper:]' <<< ${filename:0:1})${filename:1}"
  touch ./public/projects/blog/article/$filename.html

  # HTML file
  echo "<!DOCTYPE html>"  > ./public/projects/blog/article/$filename.html
  echo "<html lang='fr'>"  >> ./public/projects/blog/article/$filename.html
  echo "  <head>"  >> ./public/projects/blog/article/$filename.html
  echo "    <meta charset='UTF-8' />"  >> ./public/projects/blog/article/$filename.html
  echo "    <meta name='viewport' content='width=device-width, initial-scale=1.0' />"  >> ./public/projects/blog/article/$filename.html
  echo "    <link rel='icon' type='image/png' href='../../../img/favicon.png' />"  >> ./public/projects/blog/article/$filename.html
  echo "    <link rel='stylesheet' href='../../../css/main.css' />"  >> ./public/projects/blog/article/$filename.html
  echo "    <link href='../../../font/fontawesome/css/all.min.css' rel='stylesheet' />"  >> ./public/projects/blog/article/$filename.html
  echo "    <title>$filename</title>"  >> ./public/projects/blog/article/$filename.html
  echo "  </head>"  >> ./public/projects/blog/article/$filename.html
  echo "  <body id="$filename" class='blog'>"  >> ./public/projects/blog/article/$filename.html
  echo "    <aside id='aside'></aside>"  >> ./public/projects/blog/article/$filename.html
  echo "    <!-- Popup -->"  >> ./public/projects/blog/article/$filename.html
  echo "    <div id='popup-section'></div>"  >> ./public/projects/blog/article/$filename.html
  echo "    <script src='../js/blog.js' type='module'></script>"  >> ./public/projects/blog/article/$filename.html
  echo "  </body>"  >> ./public/projects/blog/article/$filename.html
  echo "</html>"  >> ./public/projects/blog/article/$filename.html
done
