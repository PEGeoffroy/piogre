'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { elt } from '../../../js/tool.js';
import { parse } from '../../asciidoki/js/asciidoki.js';
var aside = document.getElementById("aside");
var body = document.body;
var pageName = /\/([^\/]+).html/.exec(window.location.href)[1];
var resultTag;
function get() {
    return __awaiter(this, void 0, void 0, function () {
        var articleAdoc, text, main, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 4, , 5]);
                    return [4, fetch("http://localhost:3000/public/projects/blog/article/".concat(pageName, ".adoc"), {
                            method: "get",
                            headers: {
                                "Content-Type": "application/json"
                            }
                        })];
                case 1:
                    articleAdoc = _a.sent();
                    return [4, articleAdoc.text()];
                case 2:
                    text = _a.sent();
                    main = elt("main", { "id": "main" });
                    main.appendChild(parse(text));
                    body.appendChild(main);
                    return [4, fetch("http://localhost:3000/public/projects/blog/article/tag.txt", {
                            method: "get",
                            headers: {
                                "Content-Type": "application/json"
                            }
                        })
                            .then(function (response) { return response.text(); })
                            .then(function (tags) {
                            var tagResultArray = [];
                            var articleResultArray = [];
                            tags.split('\n').forEach(function (line) {
                                if (line !== "") {
                                    var url = /(.+)\|/.exec(line)[1].trim();
                                    var name_1 = /(.+).adoc/.exec(url)[1].trim();
                                    var type = /\|(.+):/.exec(line)[1].trim();
                                    var tagArray = /:(.+)/.exec(line)[1].replace(/\s/g, '').split(",");
                                    for (var a = 0; a < tagArray.length; a++) {
                                        var tagIndex = tagResultArray.indexOf(tagArray[a]);
                                        if (tagIndex >= 0) {
                                            articleResultArray[tagIndex].push(name_1);
                                        }
                                        else {
                                            tagResultArray.push(tagArray[a]);
                                            articleResultArray.push([name_1]);
                                        }
                                    }
                                }
                            });
                            aside.innerHTML = "";
                            for (var a = 0; a < tagResultArray.length; a++) {
                                aside.appendChild(elt("section", { "additions": [
                                        elt("p", { "content": tagResultArray[a], "class": "tag-".concat(articleResultArray[a].length) })
                                    ] }));
                            }
                        })];
                case 3:
                    _a.sent();
                    return [2, text];
                case 4:
                    error_1 = _a.sent();
                    console.error("Error:", error_1);
                    return [3, 5];
                case 5: return [2];
            }
        });
    });
}
get();
