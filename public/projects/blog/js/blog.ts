'use strict';

import { Element } from '../../../js/interface.js'
import { elt } from '../../../js/tool.js'

import { parse } from '../../asciidoki/js/asciidoki.js'

let aside = document.getElementById("aside");

interface Tag {
    [key: string]: Array<Link>
}

interface Category {
    name: string,
    articles: Array<Link>
}

interface Link {
    name: string,
    url: string
}

let body = document.body
let pageName = /\/([^\/]+).html/.exec(window.location.href)[1]

// let resultTag: Array<Tag> = []
let resultTag: Tag

async function get() {
    try {
        const articleAdoc = await fetch(`http://localhost:3000/public/projects/blog/article/${pageName}.adoc`, {
            method: "get",
            headers: {
                "Content-Type": "application/json"
            }
        })
        const text = await articleAdoc.text();
        let main = elt("main", {"id": "main"} as Element)
        main.appendChild(parse(text))
        body.appendChild(main)

        // Tag + category
        await fetch(`http://localhost:3000/public/projects/blog/article/tag.txt`, {
            method: "get",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(response => response.text())
            .then((tags) => {
                let tagResultArray: Array<string> = []
                let articleResultArray: Array<Array<string>> = []
                tags.split('\n').forEach(line => {
                    if (line !== "") {
                        let url = /(.+)\|/.exec(line)[1].trim()
                        let name = /(.+).adoc/.exec(url)[1].trim()
                        let type = /\|(.+):/.exec(line)[1].trim()
                        let tagArray = /:(.+)/.exec(line)[1].replace(/\s/g, '').split(",")

                        for (let a = 0; a < tagArray.length; a++) {
                            let tagIndex = tagResultArray.indexOf(tagArray[a])
                            if (tagIndex >= 0) {
                                articleResultArray[tagIndex].push(name)
                            } else {
                                tagResultArray.push(tagArray[a])
                                articleResultArray.push([name])
                            }
                        }
                    }
                })
                aside.innerHTML = ""
                for (let a = 0; a < tagResultArray.length; a++) {
                    aside.appendChild(elt("section", {"additions" : [
                        elt("p", {"content": tagResultArray[a], "class": `tag-${articleResultArray[a].length}`} as Element)
                    ]} as Element))
                }
            })
        
        return text
    } catch (error) {
        console.error("Error:", error);
    }
}

get()