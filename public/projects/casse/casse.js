let dicePile = [];

import { Dice } from './class/Dice.js';

for (let i = 0; i < 1; i++) {
    dicePile.push(new Dice("1", ["blue", "red", "green", "yellow", "yellow", "yellow"]));
    dicePile.push(new Dice("2", ["green", "green", "yellow", "blue", "red", "blue"]));
    dicePile.push(new Dice("3", ["green", "red", "green", "yellow", "blue", "red"]));
    dicePile.push(new Dice("4", ["blue", "yellow", "red", "yellow", "green", "red"]));
}

let stop = false;

for (let a = 0; a < 24; a++) {
    // console.log("Dé 1");
    for (let b = 0; b < 24; b++) {
        // console.log("Dé 2");
        for (let c = 0; c < 24; c++) {
            // console.log("Dé 3");
            for (let d = 0; d < 24; d++) {
                console.log("Dé 4");
                if ((
                    dicePile[0].top !== dicePile[1].top && dicePile[0].top !== dicePile[2].top && dicePile[0].top !== dicePile[3].top &&
                    dicePile[1].top !== dicePile[2].top && dicePile[1].top !== dicePile[3].top &&
                    dicePile[2].top !== dicePile[3].top
                ) && (
                    dicePile[0].front !== dicePile[1].front && dicePile[0].front !== dicePile[2].front && dicePile[0].front !== dicePile[3].front &&
                    dicePile[1].front !== dicePile[2].front && dicePile[1].front !== dicePile[3].front &&
                    dicePile[2].front !== dicePile[3].front
                ) && (
                    dicePile[0].down !== dicePile[1].down && dicePile[0].down !== dicePile[2].down && dicePile[0].down !== dicePile[3].down &&
                    dicePile[1].down !== dicePile[2].down && dicePile[1].down !== dicePile[3].down &&
                    dicePile[2].down !== dicePile[3].down
                ) && (
                    dicePile[0].back !== dicePile[1].back && dicePile[0].back !== dicePile[2].back && dicePile[0].back !== dicePile[3].back &&
                    dicePile[1].back !== dicePile[2].back && dicePile[1].back !== dicePile[3].back &&
                    dicePile[2].back !== dicePile[3].back
                )) {
                    console.log(`dice 1 : T-${dicePile[0].top} | F-${dicePile[0].front} | R-${dicePile[0].right}`);
                    console.log(`dice 2 : T-${dicePile[1].top} | F-${dicePile[1].front} | R-${dicePile[1].right}`);
                    console.log(`dice 2 : T-${dicePile[2].top} | F-${dicePile[2].front} | R-${dicePile[2].right}`);
                    console.log(`dice 3 : T-${dicePile[3].top} | F-${dicePile[3].front} | R-${dicePile[3].right}`);
                    stop = true;
                    break;
                }
                dicePile[3].nextMove();
            }
            if (stop) break;
            dicePile[2].nextMove();
        }
        if (stop) break;
        dicePile[1].nextMove();
    }
    if (stop) break;
    dicePile[0].nextMove();
}

// dicePile[0].createHTMLDice();
// dicePile[0].generate();
// dicePile[1].createHTMLDice();
// dicePile[1].generate();
// dicePile[2].createHTMLDice();
// dicePile[2].generate();
// dicePile[3].createHTMLDice();
// dicePile[3].generate();