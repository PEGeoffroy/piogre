"use strict";

let subjects = ["artefact", "lieu", "personnage"];

let adjectives = [
  {
    name: "taille",
    content: ["miniscule", "petit", "grand", "gigantesque"],
  },
];

let artefactAdjectives = ["puissant", "caché", "inconnu", "dangereux", "piégé"];
let characterAdjectives = ["grand", "discret", "inconnu", "l'air dangereux"];
