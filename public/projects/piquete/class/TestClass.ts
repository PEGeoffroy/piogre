"use strict";

export class TestClass {
  value: number

  constructor(value: number) {
    this.value = value;
  }

  increment() {
    this.value++
    this.display()
  }
  decrement() {
    this.value--
    this.display()
  }

  display() {
    console.log(this.value)
  }
}
