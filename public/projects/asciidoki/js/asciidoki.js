'use strict';
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
import { elt } from '../../../js/tool.js';
var taAsciidoc = document.getElementById("textarea-asciidoc");
var display = document.getElementById("display");
var sectionLineNumber = document.getElementById("lineNumber");
var allRegex = [
    { "type": "h1", "rule": /^=\s(.+)/ },
    { "type": "h2", "rule": /^==\s(.+)/ },
    { "type": "h3", "rule": /^===\s(.+)/ },
    { "type": "h4", "rule": /^====\s(.+)/ },
    { "type": "h5", "rule": /^=====\s(.+)/ },
    { "type": "h6", "rule": /^======\s(.+)/ },
    { "type": "ul", "rule": /^\*\s(.+)/, "level": 0 },
    { "type": "ul", "rule": /^\*\*\s(.+)/, "level": 1 },
    { "type": "ul", "rule": /^\*\*\*\s(.+)/, "level": 2 },
    { "type": "ul", "rule": /^\*\*\*\*\s(.+)/, "level": 3 },
    { "type": "ol", "rule": /^\.\s(.+)/, "level": 0 },
    { "type": "ol", "rule": /^\.\.\s(.+)/, "level": 1 },
    { "type": "ol", "rule": /^\.\.\.\s(.+)/, "level": 2 },
    { "type": "ol", "rule": /^\.\.\.\.\s(.+)/, "level": 3 },
    { "type": "note", "rule": /^>\s(.+)/ },
    { "type": "codeEnd", "rule": /^```(end)/ },
    { "type": "code", "rule": /^```(.+)/ }
];
var regexElt = [
    { "type": "italic_bold", "rule": /\*\*\*([^\*]+)\*\*\*/, "tag": "<strong>$1</strong>" },
    { "type": "bold", "rule": /\*\*([^\*]+)\*\*/, "tag": "<b>$1</b>" },
    { "type": "italic", "rule": /\*([^\*]+)\*/, "tag": "<i>$1</i>" },
    { "type": "link", "rule": /(\S+)\[(.+)\]/, "tag": "<a href='$1'>$2</a>" },
    { "type": "code", "rule": /`([^`]+)`/, "tag": "<mark>$1</mark>" }
];
if (taAsciidoc !== null) {
    taAsciidoc.onscroll = function () {
        sectionLineNumber.scrollTop = taAsciidoc.scrollTop;
    };
    taAsciidoc.addEventListener("input", function () {
        parseEditor(taAsciidoc.value);
    });
    parseEditor(taAsciidoc.value);
}
function parseEditor(text) {
    var result = [];
    var lines = text.split('\n');
    result = getAllClassifiedLine(lines);
    display.innerHTML = "";
    for (var i = 0; i < result.length; i++) {
        if (result[i].type === "ul" || result[i].type === "ol") {
            var list = getAllList(result, i);
            display.appendChild(displayList(list.list));
            i = list.id;
        }
        else if (result[i].type === "code") {
            var code = getCodeBlock(result, i);
            display.appendChild(displayCode(code.list));
            i = code.id;
        }
        else if (result[i].type === "note") {
            display.appendChild(elt("div", { "class": "note", "additions": [
                    elt("p", { "innerHTML": parseLine(result[i].value) })
                ] }));
        }
        else if (result[i].type !== "split") {
            display.appendChild(elt(result[i].type, { "innerHTML": parseLine(result[i].value) }));
        }
    }
    ;
    displayNumber(lines.length, result);
}
export function parse(text) {
    var result = [];
    var lines = text.split('\n');
    var divResult = document.createElement("div");
    result = getAllClassifiedLine(lines);
    if (result[0].type !== "h1") {
        result.shift();
    }
    for (var i = 0; i < result.length; i++) {
        if (result[i].type === "ul" || result[i].type === "ol") {
            var list = getAllList(result, i);
            divResult.appendChild(displayList(list.list));
            i = list.id;
        }
        else if (result[i].type === "code") {
            var code = getCodeBlock(result, i);
            divResult.appendChild(displayCode(code.list));
            i = code.id;
        }
        else if (result[i].type === "note") {
            divResult.appendChild(elt("div", { "class": "note", "additions": [
                    elt("p", { "innerHTML": parseLine(result[i].value) })
                ] }));
        }
        else if (result[i].type !== "split") {
            divResult.appendChild(elt(result[i].type, { "innerHTML": parseLine(result[i].value) }));
        }
    }
    ;
    return divResult;
}
function getAllClassifiedLine(lines) {
    var e_1, _a;
    var result = [];
    var listIteration = 0;
    try {
        for (var lines_1 = __values(lines), lines_1_1 = lines_1.next(); !lines_1_1.done; lines_1_1 = lines_1.next()) {
            var line = lines_1_1.value;
            var findRegex = false;
            for (var i = 0; i < allRegex.length; i++) {
                var element = allRegex[i]["rule"].exec(line);
                if (element) {
                    findRegex = true;
                    if (allRegex[i].type === "ol" || allRegex[i].type === "ul") {
                        result.push({ "type": allRegex[i].type, "value": element[1], "level": allRegex[i].level, "id": listIteration });
                        listIteration++;
                    }
                    else {
                        result.push({ "type": allRegex[i].type, "value": element[1] });
                        listIteration = 0;
                    }
                    break;
                }
            }
            if (!findRegex && line.trim().length !== 0) {
                result.push({ "type": "p", "value": line });
                listIteration = 0;
            }
            else if (line.trim().length === 0) {
                result.push({ "type": "split" });
                listIteration = 0;
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (lines_1_1 && !lines_1_1.done && (_a = lines_1["return"])) _a.call(lines_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return result;
}
function getAllList(list, position) {
    var listElt = [];
    var i = position;
    for (i; i < list.length; i++) {
        if (list[i].type === "ul" || list[i].type === "ol") {
            listElt.push(list[i]);
        }
        else {
            break;
        }
    }
    return {
        "id": i,
        "list": listElt
    };
}
function displayList(list) {
    var listElt = elt(list[0].type, {});
    for (var i = 0; i < list.length; i++) {
        if (list[i].level === 0) {
            listElt.appendChild(displayListElt(list, i));
        }
    }
    return listElt;
}
function displayListElt(list, eltId) {
    var li = elt("li", { "innerHTML": parseLine(list[eltId].value) });
    var children = getChildren(list, eltId, list[eltId].level + 1);
    if (children.length > 0) {
        var subList_1 = elt(children[0].type, {});
        children.forEach(function (child) {
            subList_1.appendChild(displayListElt(list, child.id));
        });
        li.appendChild(subList_1);
    }
    return li;
}
function getChildren(list, previousId, childrenLevel) {
    var children = [];
    for (var i = previousId + 1; i < list.length; i++) {
        if (list[i].level <= list[previousId].level) {
            break;
        }
        if (list[i].level === childrenLevel) {
            children.push(list[i]);
        }
    }
    return children;
}
function getCodeBlock(list, position) {
    var listElt = [];
    var i = position;
    for (i; i < list.length; i++) {
        if (list[i].type === "codeEnd") {
            break;
        }
        else {
            listElt.push(list[i]);
        }
    }
    return {
        "id": i + 1,
        "list": listElt
    };
}
function displayCode(list) {
    var listElt = elt("section", { "class": "code ".concat(list[0].value) });
    for (var i = 1; i < list.length; i++) {
        listElt.appendChild(elt("p", { "content": list[i].value }));
    }
    return listElt;
}
function displayNumber(nb, result) {
    sectionLineNumber.innerHTML = "";
    var array = [];
    for (var i = 0; i < nb; i++) {
        array.push("".concat(i, "."));
    }
    sectionLineNumber.value = array.join('\n');
}
function parseLine(line) {
    regexElt.forEach(function (regex) {
        var ifRegex;
        while (ifRegex !== null) {
            ifRegex = regex["rule"].exec(line);
            if (ifRegex) {
                line = line.replace(regex.rule, regex.tag);
            }
            else {
                ifRegex = null;
            }
        }
    });
    return line;
}
