'use strict';

import { Element } from '../../../js/interface.js';
import { elt } from '../../../js/tool.js';

let taAsciidoc = <HTMLInputElement>document.getElementById("textarea-asciidoc");
let display = document.getElementById("display");
let sectionLineNumber = <HTMLInputElement>document.getElementById("lineNumber")

interface Line {
    type: string,
    value?: string,
    rule?: RegExp,
    level?: number,
    id?: number
}

interface CodeElt {
    type: string,
    rule: RegExp,
    tag: string
}

let allRegex: Array<Line>  = [
    {"type": "h1", "rule": /^=\s(.+)/},
    {"type": "h2", "rule": /^==\s(.+)/},
    {"type": "h3", "rule": /^===\s(.+)/},
    {"type": "h4", "rule": /^====\s(.+)/},
    {"type": "h5", "rule": /^=====\s(.+)/},
    {"type": "h6", "rule": /^======\s(.+)/},
    {"type": "ul", "rule": /^\*\s(.+)/, "level": 0},
    {"type": "ul", "rule": /^\*\*\s(.+)/, "level": 1},
    {"type": "ul", "rule": /^\*\*\*\s(.+)/, "level": 2},
    {"type": "ul", "rule": /^\*\*\*\*\s(.+)/, "level": 3},
    {"type": "ol", "rule": /^\.\s(.+)/, "level": 0},
    {"type": "ol", "rule": /^\.\.\s(.+)/, "level": 1},
    {"type": "ol", "rule": /^\.\.\.\s(.+)/, "level": 2},
    {"type": "ol", "rule": /^\.\.\.\.\s(.+)/, "level": 3},
    {"type": "note", "rule": /^>\s(.+)/},
    {"type": "codeEnd", "rule": /^```(end)/},
    {"type": "code", "rule": /^```(.+)/}
]

let regexElt: Array<CodeElt>  = [
    {"type": "italic_bold", "rule": /\*\*\*([^\*]+)\*\*\*/, "tag": "<strong>$1</strong>"},
    {"type": "bold", "rule": /\*\*([^\*]+)\*\*/, "tag": "<b>$1</b>"},
    {"type": "italic", "rule": /\*([^\*]+)\*/, "tag": "<i>$1</i>"},
    {"type": "link", "rule": /(\S+)\[(.+)\]/, "tag": "<a href='$1'>$2</a>"},
    {"type": "code", "rule": /`([^`]+)`/, "tag": "<mark>$1</mark>"}
]

if (taAsciidoc !== null) {
    taAsciidoc.onscroll = function() {
        sectionLineNumber.scrollTop = taAsciidoc.scrollTop;
    }

    // À chaque mise à jour du textarea la fonction parse est lancée
    taAsciidoc.addEventListener("input", () => {
        parseEditor(taAsciidoc.value)
    })

    parseEditor(taAsciidoc.value)
}

function parseEditor(text: string) {
    let result = [];
    let lines = text.split('\n');

    result = getAllClassifiedLine(lines)
    
    display.innerHTML = "";

    for (let i = 0; i < result.length; i++) {
        if (result[i].type === "ul" || result[i].type === "ol") {
            let list = getAllList(result, i)
            display.appendChild(displayList(list.list))
            i = list.id
        } else if (result[i].type === "code") {
            let code = getCodeBlock(result, i)
            display.appendChild(displayCode(code.list))
            i = code.id
        } else if (result[i].type === "note") {
            display.appendChild(elt("div", {"class": "note", "additions": [
                elt("p",{"innerHTML": parseLine(result[i].value)} as Element)
            ]} as Element))
        } else if (result[i].type !== "split") {
            display.appendChild(elt(result[i].type, {"innerHTML": parseLine(result[i].value)} as Element))
        }
    };

    displayNumber(lines.length, result)
}

export function parse(text: string) {
    let result = [];
    let lines = text.split('\n');
    let divResult = document.createElement("div")

    result = getAllClassifiedLine(lines)

    if (result[0].type !== "h1") {
        result.shift()
    }

    for (let i = 0; i < result.length; i++) {
        if (result[i].type === "ul" || result[i].type === "ol") {
            let list = getAllList(result, i)
            divResult.appendChild(displayList(list.list))
            i = list.id
        } else if (result[i].type === "code") {
            let code = getCodeBlock(result, i)
            divResult.appendChild(displayCode(code.list))
            i = code.id
        } else if (result[i].type === "note") {
            divResult.appendChild(elt("div", {"class": "note", "additions": [
                elt("p",{"innerHTML": parseLine(result[i].value)} as Element)
            ]} as Element))
        } else if (result[i].type !== "split") {
            divResult.appendChild(elt(result[i].type, {"innerHTML": parseLine(result[i].value)} as Element))
        }
    };

    return divResult
}

// Fonction qui transforme une ligne de string typé en asciidoc en un élément typé
// '= Titre' => {"type": "h1", "value": "Titre"}
function getAllClassifiedLine(lines: Array<string>) {
    let result = []
    let listIteration = 0

    for (const line of lines) {
        let findRegex = false;

        for (let i = 0; i < allRegex.length; i++) {
            let element = allRegex[i]["rule"].exec(line);
    
            if (element) {
                findRegex = true;
                if (allRegex[i].type === "ol" || allRegex[i].type === "ul") {
                    result.push({"type": allRegex[i].type, "value": element[1], "level": allRegex[i].level, "id": listIteration});
                    listIteration++
                } else {
                    result.push({"type": allRegex[i].type, "value": element[1]});
                    listIteration = 0
                }
                break
            }
        }

        if (!findRegex && line.trim().length !== 0) {
            result.push({"type": "p", "value": line});
            listIteration = 0
        } else if (line.trim().length === 0) {
            result.push({"type": "split"});
            listIteration = 0
        }
    }
    return result
}

function getAllList(list: Array<Line>, position: number) {
    let listElt: Array<Line> = []
    let i = position
    
    for (i; i < list.length; i++) {
        if (list[i].type === "ul" || list[i].type === "ol") {
            listElt.push(list[i])
        } else {
            break
        }
    }
    return {
        "id": i,
        "list": listElt
    }
}

// Méthode d'initialisation de la liste
function displayList(list: Array<Line>) {
    let listElt = elt(list[0].type, {} as Element)
    
    for (let i = 0; i < list.length; i++) {
        if (list[i].level === 0) {
            listElt.appendChild(displayListElt(list, i))
        }
    }
    
    return listElt
}

// Fonction recursive qui vise à afficher un élément et qui fait appel à une autre fonction pour récupérer ses enfants
function displayListElt(list: Array<Line>, eltId: number) {
    let li = elt("li", {"innerHTML": parseLine(list[eltId].value)} as Element)
    let children = getChildren(list, eltId, list[eltId].level + 1)

    if (children.length > 0) {
        let subList = elt(children[0].type, {} as Element)
        children.forEach(child => {
            subList.appendChild(displayListElt(list, child.id))
        });
        li.appendChild(subList)
    }
    return li
}

// Fonction qui donne tous les enfant direct d'un élément
function getChildren(list: Array<Line>, previousId: number, childrenLevel: number) {
    let children = []
    
    // Pour chaque itération de la liste entre le suivant (previous + 1) avec un maximum de
    for (let i = previousId + 1; i < list.length; i++) {   
        // Si l'élément suivant (previous + 1) a un niveau inférieur ou égal alors ce n'est pas un enfant
        if (list[i].level <= list[previousId].level) {
            break
        }
        if (list[i].level === childrenLevel) {
            children.push(list[i])
        }
    }

    return children
}

function getCodeBlock(list: Array<Line>, position: number) {
    let listElt: Array<Line> = []
    let i = position
    
    for (i; i < list.length; i++) {
        if (list[i].type === "codeEnd") {
            break
        } else {
            listElt.push(list[i])
        }
    }
    return {
        "id": i + 1,
        "list": listElt
    }
}

function displayCode(list: Array<Line>) {
    let listElt = elt("section", {"class": `code ${list[0].value}`} as Element)
    
    for (let i = 1; i < list.length; i++) {
        listElt.appendChild(elt("p", {"content": list[i].value} as Element))
    }
    
    return listElt
}

function displayNumber(nb: number, result: Array<Line>) {
    sectionLineNumber.innerHTML = ""
    let array = [];
    for (let i = 0; i < nb; i++) {
        array.push(`${i}.`)
    }
    sectionLineNumber.value = array.join('\n');
}

function parseLine(line: string) {
    regexElt.forEach(regex => {
        let ifRegex
        
        while (ifRegex !== null) {
            ifRegex = regex["rule"].exec(line)

            if (ifRegex) {
                line = line.replace(regex.rule, regex.tag)
            } else {
                ifRegex = null
            }
        }
    });

    return line
}
