"use strict";

// ----- \\
// Array \\
// ----- \\

let syllablesBeg = ["karib", "oub", "et", "kan", "pal", "tul", "ir", "is", "bour", "ser",
                    "brun", "plan", "tin", "mill", "cib", "char", "lav", "don", "arm", "far",
                    "dij", "arag", "pot", "bat", "or", "elat", "drip", "nam", "hil", "kam",
                    "latt", "ip", "phen", "afr", "elod", "bark", "pod", "cal", "adar", "en"];
let syllablesEnd = ["ibelle", "uwe", "ire", "ange", "ipe", "ique", "eau", "aile", "ame", "aunelle",
                    "idoine", "ette", "ande", "ulle", "aise", "aure", "oise", "use", "alise", "ouse",
                    "oure", "alix", "eve", "èse", "idul", "oud", "oum", "igue", "atte", "onde",
                    "ulla", "ourie", "ante", "inette", "ain", "eure", "aille", "alia", "ia", "orme"];
let dyes = [];
let locations = [];

let brightness = ["light", "dark", "regular"];
let sideEffects = ["Maux de tête", "saignement"]
let specificLocation = [];
let plantParts = [];
let effects = [];
let tr = {}; // translate file
// let bodyParts = ["œil", "salive", "poil", "sang", "écaille"];

// -------- \\
// Language \\
// -------- \\

let languages = ["en", "fr"];
let language = localStorage.getItem("piogre_language");
let languageBtn = document.getElementById("language-btn");

if (language === null) language = languages[0];

languageBtn.textContent = `-${language.toUpperCase()}-`;

languageBtn.addEventListener("click", () => {
    let languageIndex = languages.indexOf(language);

    languageIndex++;
    if (languageIndex >= languages.length) languageIndex = 0;

    language = languages[languageIndex];
    localStorage.setItem("piogre_language", language);
    document.location.reload();
});

let seed = localStorage.getItem("piogre_seed");
let newSeed = "";
let seedLabel = document.getElementById("seed-label");
let newSeedLabel = document.getElementById("new-seed-label");
let saveBtn = document.getElementById("save-btn");
let generateSeedBtn = document.getElementById("generate-seed-btn");
let deleteBtn = document.getElementById("delete-btn");
let helpBtn = document.getElementById("help-btn");
let downloadBtn = document.getElementById("download-btn");
let result = [];

if (seed !== null) {
    seedLabel.textContent = seed;
    seedLabel.classList.remove("hidden");
    deleteBtn.classList.remove("hidden");
    downloadBtn.classList.remove("hidden");
}

// ---- \\
// Seed \\
// ---- \\

saveBtn.addEventListener("click", () => {
    localStorage.setItem("piogre_seed", newSeed);
    document.location.reload();
});

generateSeedBtn.addEventListener("click", () => {
    newSeed = generateSeed();
    newSeedLabel.textContent = newSeed;
});

deleteBtn.addEventListener("click", () => {
    localStorage.clear();
    document.location.reload();
});

helpBtn.addEventListener("click", () => {
    window.location.href = `../html/help-${language}.html`;
});

downloadBtn.addEventListener("click", () => {
    exportToCSV();
});

fetch(`../json/${language}.json`)
    .then(response => response.json())
    .then(file => {
        tr = file;
        // let origins = [tr["plant"], tr["animal"], tr["mineral"], tr["magic"]];

        generateSeedBtn.textContent = tr["generate_new_seed"];

        languageBtn.classList.remove("hidden");
        generateSeedBtn.classList.remove("hidden");

        effects = [
            {"name": tr["paralyzing"], "rarity": 4},
            {"name": tr["flight"], "rarity": 5},
            {"name": tr["poison"], "rarity": 3},
            {"name": tr["care"], "rarity": 2},
            {"name": tr["explosive"], "rarity": 2},
            {"name": tr["dopant"], "rarity": 2},
            {"name": tr["headache"], "rarity": 1},
            {"name": tr["constipation"], "rarity": 2},
            {"name": tr["vomiting"], "rarity": 1},
            {"name": tr["glue"], "rarity": 1},
            {"name": tr["smoked"], "rarity": 1},
            {"name": tr["sleeping_pill"], "rarity": 3},
            {"name": tr["drugs"], "rarity": 3},
            {"name": tr["addiction"], "rarity": 3},
            {"name": tr["hallucination"], "rarity": 2},
            {"name": tr["night_vision"], "rarity": 2},
            {"name": tr["invisibility"], "rarity": 4},
            {"name": tr["alcohol"], "rarity": 1},
            {"name": tr["acid"], "rarity": 2},
            {"name": tr["flammable"], "rarity": 2},
            {"name": tr["noise"], "rarity": 1},
            {"name": tr["grease"], "rarity": 1},
            {"name": tr["self_timer"], "rarity": 2},
            {"name": tr["throttle"], "rarity": 2},
            {"name": tr["blindness"], "rarity": 3},
            {"name": tr["concentrator"], "rarity": 4},
            {"name": tr["thinner"], "rarity": 2},
            {"name": tr["light"], "rarity": 2},
            {"name": tr["liquefies"], "rarity": 4},
            {"name": tr["acuity"], "rarity": 3},
            {"name": tr["celerity"], "rarity": 4},
            {"name": tr["madness"], "rarity": 3},
            {"name": tr["heat"], "rarity": 2},
            {"name": tr["sleep"], "rarity": 4},
            {"name": tr["docility"], "rarity": 3},
            {"name": tr["awkwardness"], "rarity": 3},
            {"name": tr["pheromone"], "rarity": 3},
            {"name": tr["frost"], "rarity": 2},
        ];

        dyes = [tr["purple"], tr["blue"], tr["green"], tr["yellow"], tr["orange"], tr["red"], tr["black"], tr["white"], tr["grey"], tr["brown"], tr["turquoise"], tr["magenta"], tr["red_garnet"]];
        locations = [tr["forest"], tr["plain"], tr["swamp"], tr["lake"], tr["mountain"], tr["steppe"], tr["desert"], tr["town"], tr["submarine"], tr["underground"], tr["coast"], tr["river"]];
        plantParts = [tr["leaf"], tr["stem"], tr["flower"], tr["fruit"], tr["tuber"], tr["bark"], tr["root"], tr["pit"], tr["sap"]];

        if (seed !== null) {
            generateHerbarium();
        }
    })

//----- \\
// Code \\
// ---- \\

function getRandomInt(max, min = 0) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function toTwoChar(nb) {
    result = nb.toString();
    return (result.length === 1) ? `0${result}` : result;
}

function generateHerbarium(strSeed = localStorage.getItem("piogre_seed")) {
    let array = [];

    let title = ["name", "effect", "rarity", "dye", "location", "plantPart"];

    for (let i = 0; i < title.length; i++) {
        array.push(cutSeed(strSeed, i));
    }

    let timeLoop = syllablesBeg.length;

    let lengths = [syllablesEnd.length, syllablesBeg.length, effects.length, dyes.length, locations.length, plantParts.length];

    for (let i = 0; i < timeLoop; i++) {
        let component = {};

        let calcul = [syllablesBeg[array[0]].charAt(0).toUpperCase() + syllablesBeg[array[0]].slice(1).concat(syllablesEnd[array[1]]),
                      effects[array[2]]["name"],
                      effects[array[2]]["rarity"],
                      dyes[array[3]],
                      locations[array[4]],
                      plantParts[array[5]]];

        for (let y = 0; y < title.length; y++) {

            component[title[y]] = calcul[y];
            array[y]++;
            if (array[y] >= lengths[y]) array[y] = 0;
        }

        result.push(component);
    }
    displayHerbarium(result);
    displaySeedWithExplanation(strSeed);
}

function generateSeed() {
    let str = "";
    let partName1 = toTwoChar(getRandomInt(syllablesBeg.length)); // Position of the first syllable
    let partName2 = toTwoChar(getRandomInt(syllablesEnd.length)); // Position of the second syllable, there will be the deletion of the first syllable
    let effect = toTwoChar(getRandomInt(effects.length));
    let dye = toTwoChar(getRandomInt(dyes.length));
    let location = toTwoChar(getRandomInt(locations.length));
    let plantPart = toTwoChar(getRandomInt(plantParts.length));

    if (seed === null) {
        saveBtn.textContent = tr["save"];
    } else {
        saveBtn.textContent = tr["overwrite"];
    }
    saveBtn.classList.remove("hidden");
    newSeedLabel.classList.remove("hidden");

    return str.concat(partName1, partName2, effect, dye, location, plantPart);
}

let inputBtn = document.getElementById("input-btn");
let input = document.getElementById("input");

inputBtn.addEventListener("click", () => {
    if (input.value.length !== 12) {
            displayPopup(tr["twelve_characters"]);
    } else {
        if (/^\d+$/.test(input.value)) {
            localStorage.setItem("piogre_seed", input.value);
            input.value = "";
            document.location.reload();
        } else {
            displayPopup(tr["characters_not_numbers"]);
        }
    }
});

let labels = document.getElementsByClassName("label");

for (let i = 0; i < labels.length; i++) {
    labels[i].addEventListener("click", () => {
        let temp = document.createElement("input");
        temp.value = labels[i].textContent;
        document.body.appendChild(temp);
        temp.select();
        document.execCommand("copy");
        temp.remove();
        displayPopup(`${tr["seed"]} "${labels[i].textContent}" : ${tr["copied"]}`);
    });
}

let popupSection = document.getElementById('popup-section');

/**
 * Builds, displays then removes an informational pop-up
 *
 * @param string color
 * @param string text
 */
function displayPopup(text, color = "gray") {
    let div = document.createElement('div');
    let p = document.createElement('p');
    div.style.backgroundColor = color;
    p.innerHTML = text;
    div.appendChild(p);
    popupSection.appendChild(div);

    setTimeout(function () {
        div.remove();
    }, 2000);
}

function displayHerbarium(data) {
    let main = document.getElementById("main");
    main.innerHTML = "";

    let herbarium = document.createElement("table");
    let thead = document.createElement("thead");
    let tbody = document.createElement("tbody");

    let column = ["index", "name", "effect", "plant_part", "location", "dye", "rarity"];

    for (let i = 0; i < column.length; i++) {
        thead.appendChild(createElement(tr[column[i]], column[i], "th"));
    }

    herbarium.appendChild(thead);
    herbarium.appendChild(tbody);
    
    let x = true;
    let i = 1;
    data.forEach(element => {
        let row = document.createElement("tr");
        x ? row.classList.add("odd") : row.classList.add("even");
        x = !x;

        let value = [`${i}`, element.name, element.effect, element.plantPart, element.location, element.dye, element.rarity];

        for (let i = 0; i < column.length; i++) {
            row.appendChild(createElement(value[i], column[i]));
        }

        tbody.appendChild(row);
        i++;
    });
    main.appendChild(herbarium);
}

function displaySeedWithExplanation(seedStr) {
    seedLabel.innerHTML = "";
    let array = [tr["name_part_1"], tr["name_part_2"], tr["effect"], tr["plant_part"], tr["dye"], tr["rarity"]];
    
    for (let i = 0; i < seedStr.length / 2; i++) {
        let x = 2 * i;
        let a = document.createElement("a");
        a.textContent = seedStr.slice(0 + x, 2 + x);
        a.title = array[i];
        seedLabel.appendChild(a);
    }
}

function createElement(content, column, type = "td") {
    let td = document.createElement(type);
    td.textContent = content;
    td.setAttribute("class", column);

    return td;
}

function cutSeed(strSeed, nb) {
    return parseInt(strSeed.slice(nb * 2, nb * 2 + 2));
}

// https://stackoverflow.com/questions/14964035/how-to-export-javascript-array-info-to-csv-on-client-side
function exportToCSV() {
    let exportArray = [];
    exportArray.push([]);
    exportArray[0].push(tr["name"]);
    exportArray[0].push(tr["effect"]);
    exportArray[0].push(tr["plant_part"]);
    exportArray[0].push(tr["location"]);
    exportArray[0].push(tr["dye"]);
    exportArray[0].push(tr["rarity"]);
    exportArray[0].push([]);
    exportArray[0].push(language);
    exportArray[0].push(seed);
    exportArray.push([]);
    
    exportArray.push([]);
    for (let i = 0; i < result.length; i++) {
        exportArray.push([]);
        exportArray[i + 2].push(result[i]["name"]);
        exportArray[i + 2].push(result[i]["effect"]);
        exportArray[i + 2].push(result[i]["plantPart"]);
        exportArray[i + 2].push(result[i]["location"]);
        exportArray[i + 2].push(result[i]["dye"]);
        exportArray[i + 2].push(result[i]["rarity"]);
    }

    let csvContent = "data:text/csv;charset=utf-8," 
        + exportArray.map(e => e.join(",")).join("\n");
    
    let encodedUri = encodeURI(csvContent);
    let link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "herbarium.csv");
    document.body.appendChild(link); // Required for FF
    
    link.click();
}
