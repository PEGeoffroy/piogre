"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var radioGroups = document.getElementsByClassName("radio");
export function selectRadio() {
    var _loop_1 = function (i) {
        var radioElements = radioGroups[i].children;
        var _loop_2 = function (j) {
            radioElements[j].addEventListener("click", function () {
                for (var k = 0; k < radioElements.length; k++) {
                    radioElements[k].classList.remove("selected");
                }
                radioGroups[i].dataset.value = radioElements[j].dataset.value;
                console.log(radioElements[j].dataset.value);
                radioElements[j].classList.add("selected");
            });
        };
        for (var j = 0; j < radioElements.length; j++) {
            _loop_2(j);
        }
    };
    for (var i = 0; i < radioGroups.length; i++) {
        _loop_1(i);
    }
}
export function createElement(content, column, type) {
    if (type === void 0) { type = "td"; }
    var td = document.createElement(type);
    td.textContent = content;
    td.setAttribute("class", column);
    return td;
}
export function createElt(type, content, id, CSSClass, additions) {
    if (type === void 0) { type = "p"; }
    if (content === void 0) { content = null; }
    if (id === void 0) { id = null; }
    if (CSSClass === void 0) { CSSClass = null; }
    if (additions === void 0) { additions = null; }
    var elt2 = document.createElement(type);
    if (content !== null)
        elt2.textContent = content;
    if (id !== null)
        elt2.setAttribute("id", id);
    if (CSSClass !== null)
        elt2.className = CSSClass;
    if (additions !== null) {
        additions.forEach(function (element) {
            elt2.appendChild(element);
        });
    }
    return elt2;
}
export function elt(type, params) {
    var e_1, _a;
    var HTMLElt = document.createElement(type);
    if (params.content !== undefined)
        HTMLElt.textContent = params.content;
    if (params.id !== undefined)
        HTMLElt.setAttribute("id", params.id);
    if (params["class"] !== undefined)
        HTMLElt.className = params["class"];
    if (params.innerHTML !== undefined)
        HTMLElt.innerHTML = params.innerHTML;
    if (params.additions !== undefined && Array.isArray(params.additions)) {
        params.additions.forEach(function (element) {
            HTMLElt.appendChild(element);
        });
    }
    if (params.attributes !== undefined) {
        try {
            for (var _b = __values(Object.entries(params.attributes)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var _d = __read(_c.value, 2), key = _d[0], value = _d[1];
                HTMLElt.dataset[key] = value;
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b["return"])) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    }
    return HTMLElt;
}
export function displayPopup(text, skin) {
    if (skin === void 0) { skin = "dark"; }
    var popupSection = document.getElementById('popup-section');
    var div = document.createElement('div');
    var p = document.createElement('p');
    var cross = document.createElement('div');
    var i = document.createElement('i');
    i.className = "fas fa-times";
    div.classList.add(skin);
    cross.classList.add("cross");
    p.innerHTML = text;
    cross.appendChild(i);
    div.appendChild(cross);
    div.appendChild(p);
    popupSection.appendChild(div);
    cross.addEventListener("click", function () {
        div.remove();
    });
    setTimeout(function () {
        div.remove();
    }, 10000);
}
