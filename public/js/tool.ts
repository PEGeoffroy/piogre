"use strict";

import { Element } from './interface.js';

let radioGroups = document.getElementsByClassName("radio");

export function selectRadio() {
    for (let i = 0; i < radioGroups.length; i++) {
        let radioElements = radioGroups[i].children;
        for (let j = 0; j < radioElements.length; j++) {
            radioElements[j].addEventListener("click", () => {
                for (let k = 0; k < radioElements.length; k++) {
                    radioElements[k].classList.remove("selected");
                }
                (radioGroups[i] as HTMLElement).dataset.value = (radioElements[j] as HTMLElement).dataset.value;
                console.log((radioElements[j] as HTMLElement).dataset.value);
                radioElements[j].classList.add("selected");
            });
        }
    }
}

export function createElement(content: string, column: string, type = "td") {
    let td = document.createElement(type);
    td.textContent = content;
    td.setAttribute("class", column);

    return td;
}

export function createElt(type = "p", content: string = null, id: string = null, CSSClass: string = null, additions: Array<any> = null) {
    let elt2 = document.createElement(type);
    if (content !== null) elt2.textContent = content;
    if (id !== null) elt2.setAttribute("id", id);
    if (CSSClass !== null) elt2.className = CSSClass;
    if (additions !== null) {
        additions.forEach(element => {
            elt2.appendChild(element)
        });
    }

    return elt2;
}

// Change later to create element

/**
 * Create html event
 * @param type string
 * @param params Object {"content": string, "id": string, "class": string, "additions": [element], "attributes": {name(string): value(string)}}
 * @returns HTMLElement
 */
export function elt(type: string, params: Element) {
    let HTMLElt = document.createElement(type);
    if (params.content !== undefined) HTMLElt.textContent = params.content;
    if (params.id !== undefined) HTMLElt.setAttribute("id", params.id);
    if (params.class !== undefined) HTMLElt.className = params.class;
    if (params.innerHTML !== undefined) HTMLElt.innerHTML = params.innerHTML;
    // if (params.type !== undefined) HTMLElt.type = params.type;
    if (params.additions !== undefined && Array.isArray(params.additions)) {
        params.additions.forEach(element => {
            HTMLElt.appendChild(element)
        });
    }
    if (params.attributes !== undefined) {
        for (const [key, value] of Object.entries(params.attributes)) {
            HTMLElt.dataset[key] = value;
        }
    }

    return HTMLElt;
}

interface Row {
    name: string,
    effect: string,
    plantPart: string,
    location: string,
    dye: string,
    rarity: string,
}

// export function displayArray(arrayTitles: Array<string>, arrayRows: Array<Row>) {
//     let main = document.getElementById("main");
//     main.innerHTML = "";

//     let table = document.createElement("table");
//     let thead = document.createElement("thead");
//     let tbody = document.createElement("tbody");

//     arrayTitles.forEach(element => {
//         thead.appendChild(createElement(element, element.toLowerCase(), "th"));
//     });
//     main.appendChild(table);

//     table.appendChild(thead);
//     table.appendChild(tbody);
    
//     let x = true;
//     let i = 1;
//     arrayRows.forEach(element => {
//         let row = document.createElement("tr");
//         x ? row.classList.add("odd") : row.classList.add("even");
//         x = !x;

//         let value = [`${i}`, element.name, element.effect, element.plantPart, element.location, element.dye, element.rarity];

//         for (let i = 0; i < column.length; i++) {
//             row.appendChild(createElement(value[i], column[i]));
//         }

//         tbody.appendChild(row);
//         i++;
//     });
//     main.appendChild(herbarium);
// }

/**
 * Builds, displays then removes an informational pop-up
 *
 * @param string text
 * @param string skin
 */
export function displayPopup(text: string, skin = "dark") {
    let popupSection = document.getElementById('popup-section');
    let div = document.createElement('div');
    let p = document.createElement('p');
    let cross = document.createElement('div');
    let i = document.createElement('i');
    i.className = "fas fa-times";
    div.classList.add(skin);
    cross.classList.add("cross");
    p.innerHTML = text;
    cross.appendChild(i);
    div.appendChild(cross);
    div.appendChild(p);
    popupSection.appendChild(div);

    cross.addEventListener("click", () => {
        div.remove();
    });

    setTimeout(function () {
        div.remove();
    }, 10000);
}