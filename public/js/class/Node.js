"use strict";

export class Node {
  constructor(x, y, weight) {
    this.x = x;
    this.y = y;
    this.weight = weight;
  }
}
