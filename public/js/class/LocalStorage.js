"use strict";

export class LocalStorage {
  constructor(name, type) {
    this.name = name;
    this.type = type; // String, Int, Array, Object
    this.data = this.get();
  }

  get() {
    let data = JSON.parse(localStorage.getItem(this.name));
    if (data === null) {
      if (this.type === "string") this.set("")
      if (this.type === "int") this.set(0)
      if (this.type === "array") this.set([])
      if (this.type === "object") this.set({})
    }
    return data;
  }

  set(data) {
    this.data = data
    localStorage.setItem(this.name, JSON.stringify(data));
  }

  add(value, key = null) {
    // Si la clef est nulle alors on considère que c'est un Array
    // Sinon que c'est un Object
    if (key === null && this.type === "array") {
      this.data.push(value)
    } else if (this.type === "object" && key !== null) {
      this.data.key = value;
    } else {
      console.log("Type error");
    }
    this.set(this.data);
  }

  delete() {
    localStorage.removeItem(this.name);
  }
}
