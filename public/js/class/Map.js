"use strict";

import { Cell } from "./Cell.js";

let mapDiv = document.getElementById("map");

export class Map {
  constructor(height = 7, urbanism = 0) {
    this.height = height;
    this.urbanism = urbanism;
    this.map = [];
    this.extremities = []
  }

  createMap() {
    this.height = Math.pow(2, this.height) + 1;

    for (let i = 0; i < this.height; i++) {
      this.map.push([]);
      for (let y = 0; y < this.height; y++) {
          this.map[i].push(new Cell(i, y));
      }
    }
  }

  getNeighbors(centerCell, direction8 = false) {
    let array = [];

    let north = {"x": centerCell.x - 1, "y": centerCell.y};
    let south = {"x": centerCell.x + 1, "y": centerCell.y};
    let east = {"x": centerCell.x, "y": centerCell.y - 1};
    let west = {"x": centerCell.x, "y": centerCell.y + 1};

    if (this.checkValue(north)) array.push(north);
    if (this.checkValue(south)) array.push(south);
    if (this.checkValue(east)) array.push(east);
    if (this.checkValue(west)) array.push(west);

    if (direction8) {
      let northWest = {"x": centerCell.x -1, "y": centerCell.y - 1};
      let northEast = {"x": centerCell.x -1, "y": centerCell.y + 1};
      let southWest = {"x": centerCell.x +1, "y": centerCell.y - 1};
      let southEast = {"x": centerCell.x +1, "y": centerCell.y + 1};
  
      if (this.checkValue(northWest)) array.push(northWest);
      if (this.checkValue(northEast)) array.push(northEast);
      if (this.checkValue(southWest)) array.push(southWest);
      if (this.checkValue(southEast)) array.push(southEast);
    }

    return array;
  }

  checkValue(cell, max = this.height - 1) {
    if (cell["x"] >= 0 && cell["x"] <= max && cell["y"] >= 0 && cell["y"] <= max) {
        return true;
    }
    return false;
  }

  displaySquareMap() {
    mapDiv.innerHTML = "";
    for (let x = 0; x < this.map.length; x++) {
      let rowDiv = document.createElement("div");
      rowDiv.classList.add("row");
      for (let y = 0; y < this.map[0].length; y++) {
        let cellDiv = document.createElement("div");
        cellDiv.classList.add("cell");
        cellDiv.addEventListener("click", () => {
          let tool = document.getElementById("tool").dataset.value;
          if (tool === "node") {
            if (cellDiv.classList.contains("selected")) {
              cellDiv.classList.remove("selected");
              cellDiv.classList.add("cell");
              for (let i = 0; i < this.extremities.length; i++) {
                if (this.extremities[i]["x"] === x && this.extremities[i]["y"] === y) {
                  this.extremities.splice(i, 1);
                }
              }
            } else {
              if (this.extremities.length < 2) {
                if (this.extremities.length === 0) this.map[x][y].distance = 0;
                this.extremities.push(this.map[x][y]);
                cellDiv.classList.add("selected");
              }
            }
          } else if (tool === "wall") {
            if (cellDiv.classList.contains("wall")) {
              this.map[x][y].obstacle = false;
              cellDiv.classList.remove("wall");
              cellDiv.classList.add("cell");
            } else {
              this.map[x][y].obstacle = true;
              cellDiv.classList.remove("cell");
              cellDiv.classList.remove("path");
              cellDiv.classList.add("wall");
            }
          }
        })
        cellDiv.id = `${x}:${y}`;
        cellDiv.dataset.depth = 0;
        cellDiv.dataset.x = x;
        cellDiv.dataset.y = y;
        rowDiv.appendChild(cellDiv);
      }
      mapDiv.appendChild(rowDiv);
    }
  }

  getDijkstraPath() {
    let path = [];
    this.extremities[0].distance = 0;
    let toProcess = [this.extremities[0]];

    let stop = false;
    for (const cell of toProcess) {
      if (stop) break;
      let neighbors = this.getNeighbors(cell, this.map.length - 1, true);
      for (const neighbor of neighbors)  {
        if (this.map[neighbor.x][neighbor.y].distance === null && !this.map[neighbor.x][neighbor.y].obstacle) {
          this.map[neighbor.x][neighbor.y].distance = cell.distance + 1;
          console.log(this.map[1][1].distance);
          if (neighbor.x === this.extremities[1].x && neighbor.y === this.extremities[1].y) {
            path.push(this.map[neighbor.x][neighbor.y])
            stop = true;
            break;
          }
          toProcess.push(this.map[neighbor.x][neighbor.y]);
        }
      }
    }
    for (let i = 0; i <= path[0].distance; i++) {
      let neighbors = this.getNeighbors(path[path.length - 1], true);
      let smallestNeighbor = null;
      for (const neighbor of neighbors)  {
        // Si le plus petit voisin est null ou que sa distance est nulle et que la distance ciblé est plus petite
        if ((smallestNeighbor === null || smallestNeighbor.distance !== null
            && this.map[neighbor.x][neighbor.y].distance < smallestNeighbor.distance)
            && this.map[neighbor.x][neighbor.y].distance !== null
        ) {
          smallestNeighbor = this.map[neighbor.x][neighbor.y];
        }
      }
      path.push(smallestNeighbor);
      console.log(smallestNeighbor);
      if (smallestNeighbor.x === this.extremities[0]["x"] && smallestNeighbor.y === this.extremities[0]["y"]) {
        break;
      }
    }
    console.log(this.map[1][1]);
    console.log(path);
    path.pop();
    path.shift();
    path.forEach(cell => {
      document.getElementById(`${cell.x}:${cell.y}`).className = "path";
    });
    this.initializeDistance();
  }

  initializeDistance() {
    for (const row of this.map) {
      for (const cell of row) {
        cell.distance = null;
      }
    }
  }
}
