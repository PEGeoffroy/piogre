"use strict";

import { elt, displayPopup } from "../tool.js";

let body = document.body;

export class API {
  constructor() {
    this.APIKey = localStorage.getItem("api_key");
    this.username = localStorage.getItem("username");
    this.userId = localStorage.getItem("userId");
    this.users = [];
    this.init();
  }

  // Initialise la connexion, vérifie que l'utilisateur est connecté, sinon il affiche un input de connexion
  init() {
    this.getList("37935");
    body.appendChild(elt("div", { id: "connection" }));
    this.checkConnection();
  }

  checkConnection() {
    let div = document.getElementById("connection");
    div.innerHTML = "";
    this.get("37935", this.userId).then((data) => {
      if (data === "error") {
        console.log(data);
        div.appendChild(elt("i", { class: "fas fa-lock" }));
        div.appendChild(elt("input", { type: "text", id: "username-input" }));
        div.appendChild(elt("input", { type: "password", id: "APIKey-input" }));
        let btn = elt("div", {
          class: "btn-icon",
          additions: [elt("i", { class: "fas fa-sign-in-alt" })],
        });
        btn.addEventListener("click", () => {
          let APIKeyInput = document.getElementById("APIKey-input");
          let usernameInput = document.getElementById("username-input");
          if (
            APIKeyInput !== null &&
            APIKeyInput.value.length > 0 &&
            usernameInput !== null &&
            usernameInput.value.length > 0
          ) {
            this.APIKey = APIKeyInput.value;
            localStorage.setItem("api_key", this.APIKey);
            let usernameExist = false;
            this.users.forEach((element) => {
              console.log(`${element.name} : ${usernameInput.value}`);
              if (element.name === usernameInput.value) {
                this.username = usernameInput.value;
                this.userId = element.id;
                localStorage.setItem("username", this.username);
                localStorage.setItem("userId", this.userId);
                this.checkConnection();
                usernameExist = true;
              }
            });
            if (!usernameExist) {
              displayPopup("username inexistant");
            }
          }
        });
        div.appendChild(btn);
        this.cleanLocalStorage();
        displayPopup("error");
      } else {
        console.log(data);
        div.appendChild(elt("i", { class: "fas fa-unlock-alt" }));
        div.classList.add("login");
        div.addEventListener("click", () => {
          this.cleanLocalStorage();
          this.checkConnection();
        });
        displayPopup("Connected");
      }
    });
  }

  async get(table, id) {
    try {
      const response = await fetch(
        `https://api.baserow.io/api/database/rows/table/${table}/${id}/?user_field_names=true`,
        {
          method: "GET",
          headers: {
            Authorization: `Token ${this.APIKey}`,
          },
        }
      );
      const json = await response.json();
      console.log(json);
      return json;
    } catch (error) {
      console.error("Error:", error);
      return "error";
    }
  }

  async getList(table) {
    try {
      const response = await fetch(
        `https://api.baserow.io/api/database/rows/table/${table}/?user_field_names=true`,
        {
          method: "GET",
          headers: {
            Authorization: "Token osH2J9eURBG3dCKdykt9vajiH8LQoKbi",
          },
        }
      );
      const json = await response.json();
      console.log(json.results);
      json.results.forEach((result) => {
        this.users.push({ id: result.id, name: result.name });
      });
    } catch (error) {
      console.error("Error:", error);
    }
  }

  cleanLocalStorage() {
    localStorage.removeItem("api_key", null);
    localStorage.removeItem("username", null);
    localStorage.removeItem("userId", null);
  }
}

// import { logToAPI, getAll, checkIfConnected } from './api.js';
// import { createElt } from './tool.js';

// checkIfConnected();

// let APIKey = localStorage.getItem("piogre_api_key");
// let playerName = localStorage.getItem("piogre_player_name");
// let loginElement = document.getElementById("login");
// let inputAPIKey = document.getElementById("api-key");
// let inputName = document.getElementById("name");
// let btn = document.getElementById("btn");

// if (APIKey === null || playerName === null) {
//     console.log(APIKey);
//     loginElement.classList.remove("hidden");
// } else {
//     document.body.appendChild(createElt("p", "Vous êtes connecté !", null, "connected"));
// }

// btn.addEventListener("click", () => {
//     logToAPI(inputAPIKey.value, inputName.value)
//         .then(() => {
//             document.location.reload();
//         });
//     localStorage.setItem("piogre_api_key", inputAPIKey.value);
//     localStorage.setItem("piogre_player_name", inputName.value);
// });

// // getAll(character);

// "use strict";

// import { createElt } from './tool.js';

// export async function getAll(tab) {
//     try {
//         const response = await fetch(`https://v1.nocodeapi.com/guepe/google_sheets/peIaSPHXbfsVfwWy?tabId=${tab}`, {
//             method: "get",
//             headers: {
//                 "Content-Type": "application/json"
//             }
//         });
//         const json = await response.json();
//         console.log("Success:", json);
//         return json
//     } catch (error) {
//         console.error("Error:", error);
//     }
// }

// let date = new Date();
// let day = date.toLocaleDateString();
// let hour = date.toLocaleTimeString();

// export async function logToAPI(APIKey, name) {
//     try {
//         const response = await fetch(`https://v1.nocodeapi.com/guepe/google_sheets/peIaSPHXbfsVfwWy?tabId=log&api_key=${APIKey}`, {
//             method: "post",
// 	        body: JSON.stringify([[`${day} ${hour}`, name]]),
//             headers: {
//                 "Content-Type": "application/json",
//             }
//         });
//         const json = await response.json();
//         console.log(json);
//     } catch (error) {
//         console.error("Error:", error);
//     }
// }

// export async function add(APIKey, tab, arrayValue) {
//     try {
//         const response = await fetch(`https://v1.nocodeapi.com/guepe/google_sheets/peIaSPHXbfsVfwWy?tabId=${tab}&api_key=${APIKey}`, {
//             method: "post",
// 	        body: JSON.stringify([arrayValue]),
//             headers: {
//                 "Content-Type": "application/json",
//             }
//         });
//         const json = await response.json();
//         console.log(json);
//     } catch (error) {
//         console.error("Error:", error);
//     }
// }

// export function getApiKey() {
//     return localStorage.getItem("piogre_api_key");
// }

// export function checkIfConnected() {

// }

// "use strict";

// let characterTab = "5349";
// let APIKey = "VCA3dU2yVXKDJowk1h7czEWETdg7FKEJ";

// export async function getRows(tab) {
//     const response = await fetch(`https://api.baserow.io/api/database/rows/table/${tab}/`, {
//         method: "get",
//         headers: {
//             Authorization: `Token ${APIKey}`
//         }
//     });
//     const json = await response.json();
//     return json["results"]
// }

// export async function addRow(tab, name, profession, body, spirit) {
//     const response = await fetch(`https://api.baserow.io/api/database/rows/table/${tab}/`, {
//         method: "POST",
//         headers: {
//             Authorization: `Token ${APIKey}`,
//             "Content-Type": "application/json"
//         },
//         body:  JSON.stringify({
//             "field_22424": name,
//             "field_22427": profession,
//             "field_22459": body,
//             "field_22460": spirit
//         })
//     });
//     const json = await response.json();
//     return json;
// }

// getRows(characterTab)
//     .then(data => {
//         console.table(data);
//     });

// addRow(characterTab, "Shurshalee", "Sorcerer", 1, 3);
