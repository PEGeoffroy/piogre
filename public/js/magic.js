"use strict";

import { selectRadio, displayPopup } from './tool.js';
import { getApiKey, add, checkIfConnected } from './api.js';

selectRadio();
checkIfConnected();

let saveBtn = document.getElementById("save");

saveBtn.addEventListener("click", () => {
    let name = document.getElementById("name");
    
    if (name.value.length > 0) {
        let description = document.getElementById("description");
        let notes = document.getElementById("notes");

        let result = [
            name.value,
            document.getElementById("applicationType").dataset.value,
            document.getElementById("damage").dataset.value,
            document.getElementById("effect").dataset.value,
            document.getElementById("distance").dataset.value,
            document.getElementById("area").dataset.value,
            document.getElementById("duration").dataset.value,
            document.getElementById("motion").dataset.value,
            document.getElementById("level").dataset.value,
            document.getElementById("testType").dataset.value,
            document.getElementById("testDifficulty").dataset.value,
            document.getElementById("actionCost").dataset.value,
            description.value,
            notes.value,
            document.getElementById("magicType").dataset.value
        ];

        add(getApiKey(), "spellbook", result)
            .then(() => {
                displayPopup(`Le sort "${name.value}" a correctement été ajouté.`);
                name.value = "";
                description.value = "";
                notes.value = "";
                // window.scrollTo(0,0);
                // document.location.reload();
            });
    } else {
        displayPopup("J'ai besoin d'un nom !");
    }
});
