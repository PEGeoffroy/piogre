"use strict";

let characterTab = "5349";
let APIKey = "VCA3dU2yVXKDJowk1h7czEWETdg7FKEJ";

export async function getRows(tab) {
    const response = await fetch(`https://api.baserow.io/api/database/rows/table/${tab}/`, {
        method: "get",
        headers: {
            Authorization: `Token ${APIKey}`
        }
    });
    const json = await response.json();
    return json["results"]
}

export async function addRow(tab, name, profession, body, spirit) {
    const response = await fetch(`https://api.baserow.io/api/database/rows/table/${tab}/`, {
        method: "POST",
        headers: {
            Authorization: `Token ${APIKey}`,
            "Content-Type": "application/json"
        },
        body:  JSON.stringify({
            "field_22424": name,
            "field_22427": profession,
            "field_22459": body,
            "field_22460": spirit
        })
    });
    const json = await response.json();
    return json;
}

getRows(characterTab)
    .then(data => {
        console.table(data);
    });

// addRow(characterTab, "Shurshalee", "Sorcerer", 1, 3);