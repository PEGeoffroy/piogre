export interface Element {
    type: string,
    content: string,
    id: string,
    class: string,
    additions: Array<HTMLElement>,
    attributes: string,
    innerHTML: string
}