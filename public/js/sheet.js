"use strict";

// Faire apparaître tout les competence ou juste celle que l'on possède
// Faire une fonction pour faire apparaître les competence
// Ajouter les traits
// Faire un click sur les skill et les caractéristique pour copier le code dice parser
// Ajouter l'équipement
// Ajouter les traits et compétences raciaux + nom de race
// Ajouter le nom + input
// Ajouter le up et le down des skills
// Fusionner le local storage
// Faire le stuff et l'argent
// Modifier le title avec le nom

import { createElt } from './tool.js';

let body = document.body;
let main = document.getElementById("characteristic");
let update = document.getElementById("update");

let groupGeneric = document.createElement("div");
let groupSpirit = document.createElement("div");
let groupBody = document.createElement("div");

// Get storage
let dataStorage = JSON.parse(localStorage.getItem("characterSheet"));

// If dataStorage is null init it
if (dataStorage === null) {
    localStorage.setItem("characterSheet", JSON.stringify({
        "name": "",
        "race": "",
        "skills": [],
        "traits": [],
        "characteristics": {
            "observation": 0,
            "discretion": 0,
            "charisme": 0,
            "corps": 0,
            "def-corps": 0,
            "esprit": 0,
            "def-esprit": 0,
            "endurance": 0
        },
        "settings": {
            "modDisplay": true
        }
    }));
    document.location.reload();
}

function displayModifiers(icon, label, value) {
    let char = createElt("p", value);

    let plus = createElt("i", null, null, "modifier plus fas fa-plus-circle fa-2x");
    let minus = createElt("i", null, null, "modifier minus fas fa-minus-circle fa-2x");

    if (label !== "endurance" && dataStorage["settings"]["modDisplay"]) {
        plus.classList.add("hidden");
        minus.classList.add("hidden");
    }

    plus.addEventListener("click", () => {
        dataStorage["characteristics"][label] += 1;
        localStorage.setItem("characterSheet", JSON.stringify(dataStorage));
        document.location.reload();
    });

    minus.addEventListener("click", () => {
        dataStorage["characteristics"][label] -= 1;
        localStorage.setItem("characterSheet", JSON.stringify(dataStorage));
        document.location.reload();
    });

    let result = createElt("div", null, label, "char", [
        createElt("label", `${label.substring(0, 2).toUpperCase()}.`),
        createElt("div", null, null, null, [createElt("div", null, null, null, [char])]),
        createElt("aside", null, null, null, [createElt("i", null, null, icon)]),
        plus, minus
    ]);
    return result;
}

function displayAllSkills() {
}

function addSkill(icon, label, value) {
}

function addTrait(icon, label, value) {
}

// Event listener to display button less and minus
update.addEventListener("click", () => {
    let modifiers = document.getElementsByClassName("modifier");
    for (let i = 0; i < modifiers.length; i++) {
        if (dataStorage["settings"]["modDisplay"]) {
            modifiers[i].classList.remove("hidden");
        } else {
            if (modifiers[i].parentElement.id !== "endurance") modifiers[i].classList.add("hidden");
        }
    }
    dataStorage["settings"]["modDisplay"] = !dataStorage["settings"]["modDisplay"];
    localStorage.setItem("characterSheet", JSON.stringify(dataStorage));
});

groupGeneric.appendChild(displayModifiers("fas fa-eye fa-3x", "observation", dataStorage["characteristics"]["observation"]));
groupGeneric.appendChild(displayModifiers("fas fa-user-ninja fa-3x", "discretion", dataStorage["characteristics"]["discretion"]));
groupGeneric.appendChild(displayModifiers("fas fa-theater-masks fa-3x", "charisme", dataStorage["characteristics"]["charisme"]));
groupBody.appendChild(displayModifiers("fas fa-male fa-3x", "corps", dataStorage["characteristics"]["corps"]));
groupBody.appendChild(displayModifiers("fas fa-shield-alt fa-3x", "def-corps", dataStorage["characteristics"]["def-corps"]));
groupSpirit.appendChild(displayModifiers("fas fa-fire-alt fa-3x", "esprit", dataStorage["characteristics"]["esprit"]));
groupSpirit.appendChild(displayModifiers("fas fa-shield-alt fa-3x", "def-esprit", dataStorage["characteristics"]["def-esprit"]));

main.appendChild(groupGeneric);
main.appendChild(groupBody);
main.appendChild(displayModifiers("fas fa-bolt fa-3x", "endurance", `${dataStorage["characteristics"]["endurance"]}/${dataStorage["characteristics"]["corps"] + dataStorage["characteristics"]["esprit"]}`));
main.appendChild(groupSpirit);
