"use strict";

// a: empty
// 0: point
// 1: full
// 2: top
// 3: right
// 4: bottom
// 5: left
// 6: corner top-right
// 7: corner right-bottom
// 8: corner bottom-left
// 9: corner left-top

let main =  document.getElementById("language");

let alphabet = [];

function getRandomInt(max, min = 0) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getBoolean() {
    return getRandomInt(1) ? true : false;
}

function getSize() {
    let nbAlea = getRandomInt(2);
    let result = "";

    if (nbAlea === 0) {
        result = "short";
    } else if (nbAlea === 1) {
        result = "long";
    } else {
        result = "none";
    }
    return result;
}

function transformArrayToString(array) {
    let result = "";
    for (let i = 0; i < array.length; i++) {
        for (let y = 0; y < array.length; y++) {
            result = result.concat(array[i][y]);
        }
    }
    return result;
}

function transformStringToArray(string) {
    let motif = string.split("");
    let result = [];
    let x = 0;

    for (let i = 0; i < 5; i++) {
        result.push([]);
        for (let y = 0; y < 5; y++) {
            if (motif[x] !== "a") {
                result[i].push(parseInt(motif[x]));
            } else {
                result[i].push(motif[x]);
            }
            x++;
        }
    }
    return result;
}

function displayArray(array) {
    console.log(transformArrayToString(array).slice(0, 5));
    console.log(transformArrayToString(array).slice(5, 10));
    console.log(transformArrayToString(array).slice(10, 15));
    console.log(transformArrayToString(array).slice(15, 20));
    console.log(transformArrayToString(array).slice(20, 25));
}

function drawLetter(array) {
    let letter = document.createElement("div");
    letter.classList.add("letter");

    for (let i = 0; i < 5; i++) {
        let row = document.createElement("div");
        letter.appendChild(row);
        for (let y = 0; y < 5; y++) {
            let cell = document.createElement("div");
            cell.classList.add("cell");
            if (array[i][y] === "a") {
                cell.classList.add("empty");
            } else if (array[i][y] === 0) {
                cell.classList.add("point");
            } else if (array[i][y] === 1) {
                cell.classList.add("full");
            } else if (array[i][y] === 2) {
                cell.classList.add("top");
            } else if (array[i][y] === 3) {
                cell.classList.add("right");
            } else if (array[i][y] === 4) {
                cell.classList.add("bottom");
            } else if (array[i][y] === 5) {
                cell.classList.add("left");
            } else if (array[i][y] === 6) {
                cell.classList.add("top-right");
            } else if (array[i][y] === 7) {
                cell.classList.add("right-bottom");
            } else if (array[i][y] === 8) {
                cell.classList.add("bottom-left");
            } else if (array[i][y] === 9) {
                cell.classList.add("left-top");
            }
            row.appendChild(cell);
        }
    }

    main.appendChild(letter);
}

function checkNeighbor(x, y, array) {
    let position = [];

    if (array[x][y] !== "a" && array[x][y] !== 0) {
        if (x === 0) {
            if (array[x + 1][y] >= 1) {
                position.push("bottom");
            }
        } else if (x === 4) {
            if (array[x - 1][y] >= 1) {
                position.push("top");
            }
        } else {
            if (array[x + 1][y] >= 1) {
                position.push("bottom");
            }
            if (array[x - 1][y] >= 1) {
                position.push("top");
            }
        }
        if (y === 0) {
            if (array[x][y + 1] >= 1) {
                position.push("right");
            }
        } else if (y === 4) {
            if (array[x][y - 1] >= 1) {
                position.push("left");
            }
        } else {
            if (array[x][y + 1] >= 1) {
                position.push("right");
            }
            if (array[x][y - 1] >= 1) {
                position.push("left");
            }
        }

        if (position.length < 3) {
            if (position.includes("top") && position.includes("right")) {
                return 6;
            } else if (position.includes("right") && position.includes("bottom")) {
                return 7;
            } else if (position.includes("bottom") && position.includes("left")) {
                return 8;
            } else if (position.includes("left") && position.includes("top")) {
                return 9;
            }
        }
        if (position.length === 1) {
            if (position.includes("top")) {
                return 2;
            } else if (position.includes("right")) {
                return 3;
            } else if (position.includes("bottom")) {
                return 4;
            } else if (position.includes("left")) {
                return 5;
            }
        }
        if (array[x][y] === 1) return 1;
    }
    if (array[x][y] === 0) return 0;
    return "a";
}

function createLetter() {
    let letter = [];

    for (let i = 0; i < 5; i++) {
        letter.push([]);
        for (let y = 0; y < 5; y++) {
            letter[i].push("a");
        }
    }

    let leftPosition = getBoolean() ? "top" : "bottom";
    let centerPosition = getBoolean() ? "top" : "bottom";
    let rightPosition = getBoolean() ? "top" : "bottom";

    let leftLength = getSize();
    let centerLength = getSize();
    let rightLength = getSize();

    if (leftLength === "long") {
        letter[0][0] = 1;
        letter[1][0] = 1;
        letter[2][0] = 1;
        letter[3][0] = 1;
        letter[4][0] = 1;
    } else if (leftLength === "short") {
        if (leftPosition === "top") {
            letter[0][0] = 1;
            letter[1][0] = 1;
            letter[2][0] = 1;
        } else {
            letter[2][0] = 1;
            letter[3][0] = 1;
            letter[4][0] = 1;
        }
    };

    if (centerLength === "long") {
        letter[0][2] = 1;
        letter[1][2] = 1;
        letter[2][2] = 1;
        letter[3][2] = 1;
        letter[4][2] = 1;
    } else if (centerLength === "short") {
        if (centerPosition === "top") {
            letter[0][2] = 1;
            letter[1][2] = 1;
            letter[2][2] = 1;
        } else {
            letter[2][2] = 1;
            letter[3][2] = 1;
            letter[4][2] = 1;
        }
    };

    if (rightLength === "long") {
        letter[0][4] = 1;
        letter[1][4] = 1;
        letter[2][4] = 1;
        letter[3][4] = 1;
        letter[4][4] = 1;
    } else if (rightLength === "short") {
        if (rightPosition === "top") {
            letter[0][4] = 1;
            letter[1][4] = 1;
            letter[2][4] = 1;
        } else {
            letter[2][4] = 1;
            letter[3][4] = 1;
            letter[4][4] = 1;
        }
    };

    let topPosition = getBoolean() ? "top" : "bottom";
    let mediumPosition = getBoolean() ? "top" : "bottom";
    let bottomPosition = getBoolean() ? "top" : "bottom";

    let topLength = getSize();
    let mediumLength = getSize();
    let bottomLength = getSize();

    if (topLength === "long") {
        letter[0][0] = 1;
        letter[0][1] = 1;
        letter[0][2] = 1;
        letter[0][3] = 1;
        letter[0][4] = 1;
    } else if (topLength === "short") {
        if (topPosition === "top") {
            letter[0][0] = 1;
            letter[0][1] = 1;
            letter[0][2] = 1;
        } else {
            letter[0][2] = 1;
            letter[0][3] = 1;
            letter[0][4] = 1;
        }
    };

    if (mediumLength === "long") {
        letter[2][0] = 1;
        letter[2][1] = 1;
        letter[2][2] = 1;
        letter[2][3] = 1;
        letter[2][4] = 1;
    } else if (mediumLength === "short") {
        if (mediumPosition === "top") {
            letter[2][0] = 1;
            letter[2][1] = 1;
            letter[2][2] = 1;
        } else {
            letter[2][2] = 1;
            letter[2][3] = 1;
            letter[2][4] = 1;
        }
    };

    if (bottomLength === "long") {
        letter[4][0] = 1;
        letter[4][1] = 1;
        letter[4][2] = 1;
        letter[4][3] = 1;
        letter[4][4] = 1;
    } else if (bottomLength === "short") {
        if (bottomPosition === "top") {
            letter[4][0] = 1;
            letter[4][1] = 1;
            letter[4][2] = 1;
        } else {
            letter[4][2] = 1;
            letter[4][3] = 1;
            letter[4][4] = 1;
        }
    };

    // Verifier le vide par groupe de colonne ou de ligne côte à côte
    if (letter[0][0] === "a" && letter[0][1] === "a" &&
        letter[1][0] === "a" && letter[1][1] === "a" &&
        letter[2][0] === "a" && letter[2][1] === "a" &&
        letter[3][0] === "a" && letter[3][1] === "a" &&
        letter[4][0] === "a" && letter[4][1] === "a") {
            letter[2][0] = 0;
    }

    if (letter[0][1] === "a" && letter[0][2] === "a" && letter[0][3] === "a" &&
        letter[1][1] === "a" && letter[1][2] === "a" && letter[1][3] === "a" &&
        letter[2][1] === "a" && letter[2][2] === "a" && letter[2][3] === "a" &&
        letter[3][1] === "a" && letter[3][2] === "a" && letter[3][3] === "a" &&
        letter[4][1] === "a" && letter[4][2] === "a" && letter[4][3] === "a") {
            letter[2][2] = 0;
    }

    if (letter[0][3] === "a" && letter[0][4] === "a" &&
        letter[1][3] === "a" && letter[1][4] === "a" &&
        letter[2][3] === "a" && letter[2][4] === "a" &&
        letter[3][3] === "a" && letter[3][4] === "a" &&
        letter[4][3] === "a" && letter[4][4] === "a") {
            letter[2][4] = 0;
    }

    if (letter[0][0] === "a" && letter[1][0] === "a" &&
        letter[0][1] === "a" && letter[1][1] === "a" &&
        letter[0][2] === "a" && letter[1][2] === "a" &&
        letter[0][3] === "a" && letter[1][3] === "a" &&
        letter[0][4] === "a" && letter[1][4] === "a") {
            letter[0][2] = 0;
    }

    if (letter[1][0] === "a" && letter[2][0] === "a" && letter[3][0] === "a" &&
        letter[1][1] === "a" && letter[2][1] === "a" && letter[3][1] === "a" &&
        letter[1][2] === "a" && letter[2][2] === "a" && letter[3][2] === "a" &&
        letter[1][3] === "a" && letter[2][3] === "a" && letter[3][3] === "a" &&
        letter[1][4] === "a" && letter[2][4] === "a" && letter[3][4] === "a") {
            letter[2][2] = 0;
    }

    if (letter[3][0] === "a" && letter[4][0] === "a" &&
        letter[3][1] === "a" && letter[4][1] === "a" &&
        letter[3][2] === "a" && letter[4][2] === "a" &&
        letter[3][3] === "a" && letter[4][3] === "a" &&
        letter[3][4] === "a" && letter[4][4] === "a") {
            letter[4][2] = 0;
    }

    if (letter[1][1] === "a" && letter[2][1] === "a" && letter[3][1] === "a" &&
        letter[1][2] === "a" && letter[2][2] === "a" && letter[3][2] === "a" &&
        letter[1][3] === "a" && letter[2][3] === "a" && letter[3][3] === "a") {
            letter[2][2] = letter[2][2] = getBoolean() ? 0 : "a";
    }

    let result = [];

    for (let i = 0; i < 5; i++) {
        result.push([]);
        for (let y = 0; y < 5; y++) {
            result[i].push(checkNeighbor(i, y, letter));
        }
    }

    return result;
}

for (let i = 0; i < 150; i++) {
    let letter = createLetter();
    let string = transformArrayToString(letter);
    if (!alphabet.includes(string) && string != "aaaaaaaaaaaaaaaaaaaaaaaaa" && string != "4a7151a1aa61118aa1a1319a2") {
        alphabet.push(string);
        drawLetter(letter);
    } else {
        console.log("duplication");
        i--;
    }
}

// * [ ] Gérer les rotations d'un motif pour les éviter (manipulation d'une string) les symétrie et les deux, faire que ce soit une option
// * [ ] Facto le code
// * [ ] stocker le résultat
// * [ ] Faire du code commun en JS
// * [ ] Gérer différent style ?
// * [ ] Gérer les langues

// * [x] Transformer le 10 en a et essayer de tenir de 0 à 9 pour pouvoir transformer string to array
// * [x] Retranscrire la croix avec les embouts

// JDR groupé de masse en mode conteur, idée de Fibre