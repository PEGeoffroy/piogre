"use strict";

import { displayPopup } from './tool.js';
import { Map } from "./class/Map.js";

let map = new Map(5);
map.createMap();
map.displaySquareMap();

let runBtn = document.getElementById("run");
let nodeBtn = document.getElementById("node");
let wallBtn = document.getElementById("wall");

let toolElt = document.getElementById("tool");

runBtn.addEventListener("click", () => {
    let paths = document.getElementsByClassName("path");
    let walls = document.getElementsByClassName("wall");
    for (let i = paths.length - 1; i >= 0; i--) {
        paths[i].className = "cell";
    }
    for (let i = walls.length - 1; i >= 0; i--) {
        walls[i].className = "wall";
    }
    if (map.extremities.length === 2) {
        map.getDijkstraPath();
    } else {
        displayPopup("Sélectionner deux points");
    }
})

nodeBtn.addEventListener("click", () => {
    toolElt.innerHTML = "Selected tool: node";
    toolElt.dataset.value = "node";
})

wallBtn.addEventListener("click", () => {
    toolElt.innerHTML = "Selected tool: wall";
    toolElt.dataset.value = "wall";
})