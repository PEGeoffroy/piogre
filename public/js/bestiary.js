"use strict";

import { elt } from './tool.js';

let btnDisplaySheetGeneric = document.getElementById("btn-display-sheet-generic");
let btnDisplaySheetAddBeast = document.getElementById("btn-display-sheet-add-beast");
let btnSaveBeast = document.getElementById("btn-save-beast");
let lstScroll = document.getElementById("lst-scroll");
let sheetLoading = document.getElementById("sheet-loading");
// let btnAddSkill = document.getElementById("btn-add-skill");
// let btnSkill = document.getElementById("btn-skill");
// let iptSkill = document.getElementById("ipt-skill");
// let lstSkills = document.getElementById("lst-skill");
// let main = document.getElementById("main");
// let menuSkill = document.getElementById("menu-skill");
// let newSkill = document.getElementById("new-skill");
// let skillsSection = document.getElementById("skills");
// let menuScroll = document.getElementById("menu-scroll");

let abilities = document.getElementsByClassName("ability");
// let items = document.getElementsByClassName("item");

let skills = [];

let bestiary = [];

// let card = {
//     "name": "",
//     "ability": {},
//     "skill": [],
//     "stuff": [],
//     "loot": []
// };

// function initLstSkills() {
//     let filteredSkills = skills.filter(element => element["name"].toLowerCase().includes(iptSkill.value.toLowerCase()));
//     lstSkills.innerHTML = "";
//     filteredSkills.forEach(skill => {
//         let skillElt = elt("p", {"content": skill.name, "attributes": {"id": skill.id, "ability": skill.ability}});
//         skillElt.addEventListener("click", () => {
//             skillsSection.appendChild(elt("p", {"content": `${newSkill.firstChild.innerHTML} - ${skill.ability} - ${skill.name}`, "class": "skill"}))
//             console.log(skill);
//         })
//         lstSkills.appendChild(skillElt)
//     });
// }

// for (const i of items) {
//     i.addEventListener("click", () => {
//         console.log(i.id);
//     })
// }

// btnSkill.addEventListener("click", () => {
//     menuSkill.classList.remove("hidden");
//     btnAddSkill.classList.remove("hidden");
//     btnMenu.firstChild.className = "fas fa-times fa-2x";
//     iptSkill.value = "";
//     iptSkill.focus();
//     initLstSkills();
// })

// iptSkill.addEventListener("input", () => {
//     initLstSkills();
// })

// Récupérer les données stocké en JSON
fetch(`../json/skill.json`)
    .then(response => response.json())
    .then(file => {
        // console.log(file);
        file.forEach(skill => {
            skills.push(skill);
        });
    })
    // Puis récupérer les données stocké dans le localstorage sinon le créer
    .then(() => {
        bestiary = JSON.parse(localStorage.getItem("bestiary"));
        if (bestiary !== null) {
            bestiary.forEach(beast => {
                let item = elt("p", {"content": beast, "class": "item", "id": `beast-${beast}`});
                lstScroll.appendChild(item);
            });
        }
    })
    // Puis enlever le loader qui couvre la page
    .then(() => {
        sheetLoading.classList.add("hidden");
    })

// init navigations
let sheetBtns = document.getElementsByClassName("sheet-btn");
let sheets = document.getElementsByClassName("sheet");

for (const btn of sheetBtns) {
    btn.addEventListener("click", () => {
        for (const sheet of sheets) {
            sheet.classList.add("hidden");
        }
        if (!btn.firstChild.classList.contains("fa-times")) {
            document.getElementById(btn.dataset.sheet).classList.remove("hidden");
            btnDisplaySheetGeneric.firstChild.className = "fas fa-times fa-2x";
        } else {
            btnDisplaySheetGeneric.firstChild.className = "fas fa-bars fa-2x";
            if (btn.dataset.sheet !== "sheet-add-beast") {
                btnSaveBeast.classList.add("hidden");
            }
        }
    })
}

// init ability butons to increase
for (const i of abilities) {
    i.addEventListener("click", () => {
        let value = parseInt(i.lastChild.innerHTML) + 1;
        if (value > 5) value = 0;
        i.lastChild.innerHTML = value;
        console.log(i.id);
    })
}

btnDisplaySheetAddBeast.addEventListener("click", () => {
    btnSaveBeast.classList.remove("hidden");
})