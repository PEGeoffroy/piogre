import { logToAPI, getAll, checkIfConnected } from './api.js';
import { createElt } from './tool.js';

checkIfConnected();

let APIKey = localStorage.getItem("piogre_api_key");
let playerName = localStorage.getItem("piogre_player_name");
let loginElement = document.getElementById("login");
let inputAPIKey = document.getElementById("api-key");
let inputName = document.getElementById("name");
let btn = document.getElementById("btn");

if (APIKey === null || playerName === null) {
    console.log(APIKey);
    loginElement.classList.remove("hidden");
} else {
    document.body.appendChild(createElt("p", "Vous êtes connecté !", null, "connected"));
}

btn.addEventListener("click", () => {
    logToAPI(inputAPIKey.value, inputName.value)
        .then(() => {
            document.location.reload();
        });
    localStorage.setItem("piogre_api_key", inputAPIKey.value);
    localStorage.setItem("piogre_player_name", inputName.value);
});

// getAll(character);