"use strict";

import { API } from './class/API.js';
import { elt } from './tool.js';

let HTMLElt = elt("p", {
    "content": "Ceci est un test ",
    "id": "id",
    "class": "test",
    "additions": [
        elt("span", {"content": "span"})
    ],
    "attributes": {"key_1": "value_1", "key_2": "value_2"}
});

let api = new API();
document.body.appendChild(HTMLElt);

// https://www.jchr.be/tokipona/dico-fr-tp.htm