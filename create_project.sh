#!/bin/bash

rootName="$(tr '[:upper:]' '[:lower:]' <<< ${1:0})"
title="$(tr '[:lower:]' '[:upper:]' <<< ${rootName:0:1})${rootName:1}"

rm -rf ./public/projects/$rootName

mkdir ./public/projects/$rootName
mkdir ./public/projects/$rootName/html
mkdir ./public/projects/$rootName/css
mkdir ./public/projects/$rootName/js
mkdir ./public/projects/$rootName/js/class

touch ./public/projects/$rootName/$rootName.html
touch ./public/projects/$rootName/css/$rootName.sass
touch ./public/projects/$rootName/js/$rootName.ts
touch ./public/projects/$rootName/html/.gitkeep

# HTML file
echo "<!DOCTYPE html>"  > ./public/projects/$rootName/$rootName.html
echo "<html lang='fr'>"  >> ./public/projects/$rootName/$rootName.html
echo "  <head>"  >> ./public/projects/$rootName/$rootName.html
echo "    <meta charset='UTF-8' />"  >> ./public/projects/$rootName/$rootName.html
echo "    <meta name='viewport' content='width=device-width, initial-scale=1.0' />"  >> ./public/projects/$rootName/$rootName.html
echo "    <link rel='icon' type='image/png' href='../../img/favicon.png' />"  >> ./public/projects/$rootName/$rootName.html
echo "    <link rel='stylesheet' href='../../css/main.css' />"  >> ./public/projects/$rootName/$rootName.html
echo "    <link href='../../font/fontawesome/css/all.min.css' rel='stylesheet' />"  >> ./public/projects/$rootName/$rootName.html
echo "    <title>$title</title>"  >> ./public/projects/$rootName/$rootName.html
echo "  </head>"  >> ./public/projects/$rootName/$rootName.html
echo "  <body id="$rootName">"  >> ./public/projects/$rootName/$rootName.html
echo "    <h1>$title</h1>"  >> ./public/projects/$rootName/$rootName.html
echo "    <main id='main'></main>"  >> ./public/projects/$rootName/$rootName.html
echo "    <!-- Popup -->"  >> ./public/projects/$rootName/$rootName.html
echo "    <div id='popup-section'></div>"  >> ./public/projects/$rootName/$rootName.html
echo "    <script src='./js/$rootName.js' type='module'></script>"  >> ./public/projects/$rootName/$rootName.html
echo "  </body>"  >> ./public/projects/$rootName/$rootName.html
echo "</html>"  >> ./public/projects/$rootName/$rootName.html

# PrototypeClass.ts
echo "'use strict';"  > ./public/projects/$rootName/js/class/PrototypeClass.ts
echo ""  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "export class PrototypeClass {"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "  value: number"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo ""  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "  constructor(value: number) {"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "    this.value = value;"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "  }"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo ""  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "  increment() {"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "    this.value++"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "    this.display()"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "  }"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "  decrement() {"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "    this.value--"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "    this.display()"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "  }"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo ""  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "  display() {"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "    console.log(this.value)"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "  }"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts
echo "}"  >> ./public/projects/$rootName/js/class/PrototypeClass.ts

# TS file
echo "'use strict';"  > ./public/projects/$rootName/js/$rootName.ts
echo ""  >> ./public/projects/$rootName/js/$rootName.ts
echo "import { Element } from '../../../js/interface.js';"  >> ./public/projects/$rootName/js/$rootName.ts
echo "import { elt } from '../../../js/tool.js';"  >> ./public/projects/$rootName/js/$rootName.ts
echo ""  >> ./public/projects/$rootName/js/$rootName.ts
echo "import { PrototypeClass } from './class/PrototypeClass.js';"  >> ./public/projects/$rootName/js/$rootName.ts
echo ""  >> ./public/projects/$rootName/js/$rootName.ts
echo "let main = document.getElementById('main')"  >> ./public/projects/$rootName/js/$rootName.ts
echo ""  >> ./public/projects/$rootName/js/$rootName.ts
echo "let test = new PrototypeClass(5)"  >> ./public/projects/$rootName/js/$rootName.ts
echo ""  >> ./public/projects/$rootName/js/$rootName.ts
echo "test.increment()"  >> ./public/projects/$rootName/js/$rootName.ts
echo "test.decrement()"  >> ./public/projects/$rootName/js/$rootName.ts
echo ""  >> ./public/projects/$rootName/js/$rootName.ts
echo "main.appendChild(elt('p', {'content': (test.value).toString()}  as Element))" >> ./public/projects/$rootName/js/$rootName.ts

# SASS file
echo "h1"  > ./public/projects/$rootName/css/$rootName.sass
echo "    color: red"  >> ./public/projects/$rootName/css/$rootName.sass

# main.sass
echo "@import '../projects/$rootName/css/$rootName'"  >> ./public/css/main.sass

# index.adoc
echo "* link:./projects/$rootName/$rootName.html[$title]"  >> ./public/index.adoc

asciidoctor ./public/index.adoc
